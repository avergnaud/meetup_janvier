(function() {

    var a = 1;//private exports.a

    var exports = {};
    exports.foo = function foo(x) {
        a += x;
        return a;
    };
    exports.bar = function bar() { return a; }
    window.MyModule = exports;

//TODO cf revealing pattern https://scotch.io/bar-talk/4-javascript-design-patterns-you-should-know

})();/* IIFE */

//console.log(a)

/*
In this example, the variable a isn’t visible to the outside world, 
but you can still access the module’s public methods via MyModule.foo and MyModule.bar. 
This is often called “the module pattern”, and the approach of having an anonymous 
function called right after its definition 
is called an immediately invoked function expression or IIFE.
*/