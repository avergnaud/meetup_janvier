a = 3;         // global variable

/* window : fourni par le runtime du navigateur */
console.log(window.a);

MyModule.foo(10);

MyModule.foo(5);

console.log(MyModule.bar());

// MyModuleBIS.foo(30);
// console.log(MyModuleBIS.bar());
