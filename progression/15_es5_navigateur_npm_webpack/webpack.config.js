var webpack = require('webpack'),
path = require('path');

module.exports = {
entry: {
    main: './src/script.js'
},
output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js'
},
module: {
    loaders: []
},
plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
    })
]
};