
    var myExports = {};
    var a = 1;
    myExports.foo = function foo(x) {
        a += x;
        return a;
    };
    myExports.bar = function bar() { return a; }
    //window.MyModule = myExports;

    module.exports = myExports;