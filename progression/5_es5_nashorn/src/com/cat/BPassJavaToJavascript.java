package com.cat;

import javax.script.*;
import java.util.*;

public class BPassJavaToJavascript {
    public static void main(String[] args) throws ScriptException {

        List<Integer> numBunnies = Arrays.asList(2, 4, 8, 16);

        // create a script engine manager
        ScriptEngineManager factory = new ScriptEngineManager();
        // create a Nashorn script engine
        ScriptEngine engine = factory.getEngineByName("nashorn");

        engine.put("bunnies", numBunnies);

        // evaluate JavaScript statement
        System.out.println(engine.eval("bunnies[1]"));

    }

}