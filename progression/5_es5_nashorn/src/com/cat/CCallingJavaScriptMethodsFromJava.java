package com.cat;

import java.nio.file.*;
import java.io.*;
import javax.script.*;

public class CCallingJavaScriptMethodsFromJava {

    public static void main(String... args) throws ScriptException, NoSuchMethodException, IOException {

        Path path = Paths.get("hop_function.js");
        Reader jsFileReader = Files.newBufferedReader(path);

        // create a script engine manager
        ScriptEngineManager factory = new ScriptEngineManager();
        // create a Nashorn script engine
        ScriptEngine engine = factory.getEngineByName("nashorn");

        engine.eval(jsFileReader);
        Invocable invocable = (Invocable) engine;
        System.out.println(invocable.invokeFunction("hop", 3));

    }
}