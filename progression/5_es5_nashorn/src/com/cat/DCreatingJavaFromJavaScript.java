package com.cat;

import java.nio.file.*;
import java.io.*;
import javax.script.*;

public class DCreatingJavaFromJavaScript {

    public static void main(String... args) throws ScriptException, IOException {
        Path path = Paths.get("using_java_runtime.js");
        Reader jsFileReader = Files.newBufferedReader(path);

        // create a script engine manager
        ScriptEngineManager factory = new ScriptEngineManager();
        // create a Nashorn script engine
        ScriptEngine engine = factory.getEngineByName("nashorn");

        engine.eval(jsFileReader);
    }

}