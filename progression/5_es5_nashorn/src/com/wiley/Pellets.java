package com.wiley;

public class Pellets {
    private int theCount;

    public int getCount() {
        return theCount;
    }

    public void setCount(int count) {
        this.theCount = count;
    }
}