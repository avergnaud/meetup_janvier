print("hellol")

var Math = java.lang.Math
print(Math.random())

var l = new java.util.ArrayList()
print(l.size())

/* attention : nécessite d'avoir compilé Pellets.java */
var Pellets = Java.type('com.wiley.Pellets')
var p = new Pellets()
p.setCount(5) // setter
print(p.getCount()) // getter

var q = new com.wiley.Pellets()
q.count= 6
print(q.count)
// setter
// getter

q['count'] = 7
print(q['count'])