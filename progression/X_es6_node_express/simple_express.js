const express = require('express')

/* ??? */
let pathToModule = require.resolve('express');
console.log("pathToModule : " + pathToModule);
/* https://nodejs.org/api/modules.html#modules_require_resolve_paths_request */
console.log("paths : " + require.resolve.paths("existe_pas"))

/* serveur : */
const app = express()

app.get('/', (req, res) => res.send('Hello World!'))

app.listen(3000, () => console.log('Example app listening on port 3000!'))